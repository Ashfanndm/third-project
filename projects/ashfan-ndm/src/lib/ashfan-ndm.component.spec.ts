import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AshfanNdmComponent } from './ashfan-ndm.component';

describe('AshfanNdmComponent', () => {
  let component: AshfanNdmComponent;
  let fixture: ComponentFixture<AshfanNdmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AshfanNdmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AshfanNdmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
