import { NgModule } from '@angular/core';
import { AshfanNdmComponent } from './ashfan-ndm.component';
import { ButtonComponent } from './button/button.component';



@NgModule({
  declarations: [AshfanNdmComponent, ButtonComponent],
  imports: [
  ],
  exports: [AshfanNdmComponent, ButtonComponent]
})
export class AshfanNdmModule { }
