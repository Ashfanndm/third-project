import { TestBed } from '@angular/core/testing';

import { AshfanNdmService } from './ashfan-ndm.service';

describe('AshfanNdmService', () => {
  let service: AshfanNdmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AshfanNdmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
