import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'lib-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() value = 0;

  count = 0;

  constructor() { }

  ngOnInit(): void {
  }

  incrementCount(){
    this.count += this.value;
  }

  decrementCount(){
    this.count -= this.value;
  }

}
