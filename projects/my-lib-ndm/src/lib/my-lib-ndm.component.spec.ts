import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyLibNdmComponent } from './my-lib-ndm.component';

describe('MyLibNdmComponent', () => {
  let component: MyLibNdmComponent;
  let fixture: ComponentFixture<MyLibNdmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyLibNdmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyLibNdmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
