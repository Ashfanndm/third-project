import { NgModule } from '@angular/core';
import { MyLibNdmComponent } from './my-lib-ndm.component';
import {ButtonComponent} from '../../../ashfan-ndm/src/lib/button/button.component';



@NgModule({
  declarations: [MyLibNdmComponent, ButtonComponent],
  imports: [
  ],
  exports: [MyLibNdmComponent, ButtonComponent]
})
export class MyLibNdmModule { }
