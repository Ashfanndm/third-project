import { TestBed } from '@angular/core/testing';

import { MyLibNdmService } from './my-lib-ndm.service';

describe('MyLibNdmService', () => {
  let service: MyLibNdmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyLibNdmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
