import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MyLibSajaModule } from 'my-lib-saja';
import { MyLibNdmModule } from 'my-lib-ndm';
// @ts-ignore
import { AshfanNdmModule } from 'ashfan-ndm';




@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    MyLibSajaModule,
    MyLibNdmModule,
    AshfanNdmModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
